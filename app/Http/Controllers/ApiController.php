<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request as Request;

// Goutte
use Goutte\Client;
use Carbon\Carbon;

class ApiController extends Controller
{
	private $direction;
    private $client;
    private $apiUrl;
    private $gMapsApiKey;

    public function __construct()
    {
        $this->client 			= new Client();	
    	$this->apiUrl 			= 'https://api.skypicker.com/flights';
    	$this->gMapsApiKey 		= 'AIzaSyAoYsx2_imp0yReoUFdGkHvK2F3tZ6_EjM';
    }

    public function getTravelResults(Request $request)
    {
    	$data = $request->all();
    	$transit = $this->getGMapsDirections($data);
    	$flights = $this->getFlights($data);

    	$resultArray = ['flights' => $flights, 'transit' => $transit];
		$result = $this->getReadableOutputAndPrices($resultArray, $data);

		return response()->json($result);
    }

    private function getFlights($data)
    {
    	// Params for finding flights
    	$params = array(
    		'flyFrom' 			=> $data['from'],
    		'to'				=> $data['to'],
    		'dateFrom'			=> str_replace('-', '/', $data['dateFrom']),
    		'dateTo'			=> str_replace('-', '/', $data['dateTo']),
    		'directFlights'		=> 1, // Default value
    		'passengers'		=> $data['passengers'],
    		'partner'			=> 'picky',
    		'currency'			=> 'eur',
    		'price_to'			=> 100
    	);

    	// Build API Url
    	$requestUrl = $this->apiUrl . "?" . http_build_query($params);
    	
    	$flightCrawler = $this->client->request('GET', $requestUrl);
    	// Work around for getting json output
    	$flightCrawlerJson = json_decode($this->client->getResponse()->getContent());
    	// Cast object to array
    	$flightCrawlerResult = (array) $flightCrawlerJson;

    	// Check if result is empty
    	if($flightCrawlerResult != NULL) {
    		return $flightCrawlerResult;
    	} else {
    		return array();
    	}
    }

    private function getGMapsDirections($data)
    {
    	// Get transit directions via Google's API
    	// Our parameters
		$params = array(
			'mode'			=> 'transit',
			'origin'		=> $data['from'],
			'destination'	=> $data['to'],
			'key'			=> $this->gMapsApiKey
		);
		
		$params_string = '';
		// Join parameters into URL string
		foreach($params as $var => $val){
	   		$params_string .= '&' . $var . '=' . urlencode($val);  
		}
		
		// Request URL
		$url = "https://maps.googleapis.com/maps/api/directions/json?" . ltrim($params_string, '&');
		// Make our API request
		$request = $this->client->request("GET", $url);
		$json_data = $this->client->getResponse()->getContent();
		
		// Parse the JSON response
		$directions = json_decode($json_data);


        if($directions != NULL) {
        	return $directions->routes;
        } else {
        	return array();
        }
    }

    private function getReadableOutputAndPrices($routes, $data)
    {
    	$transit = $routes["transit"];
    	$transitData = [];

    	// Get all transit data and convert it
    	foreach ($transit as $pt) {
    		foreach ($pt->legs as $leg) {
	    		foreach ($leg->steps as $step) {
	    			if(isset($step->transit_details)) {
		    			foreach($step->transit_details->line->agencies as $agency) {
    						$agencies['agency'][] = array('name' => $agency->name, 'url' => $agency->url);

    						if(count($leg->steps) > 1) {
	    						$steps[] = array(
										'agency'			=> $agency->name,
									  	'transitMode' 		=> $step->transit_details->line->vehicle->name,
									  	'departure' 		=> $step->transit_details->departure_stop->name, 
									  	'arrival' 			=> $step->transit_details->arrival_stop->name,
									  	'departureDate'		=> date('d-m-Y H:i:s', $step->transit_details->departure_time->value),
				 					  	'arrivalDate'		=> date('d-m-Y H:i:s', $step->transit_details->arrival_time->value),
									  	'duration'			=> $step->duration->text,
									  	'price'				=> NULL // Todo, add prices!
				    			);
    						} else {
    							$steps = array();
    						}

    						$transitData['transitData'][] = array('agency'				=> $agency->name,
    															  'transitMode' 		=> $step->transit_details->line->vehicle->name,
				    											  'departure' 			=> $leg->start_address, 
				    											  'arrival' 			=> $leg->end_address,
				    											  'steps'				=> $steps,
				    											  'departureDate'		=> date('d-m-Y H:i:s', $leg->departure_time->value),
	    									 					  'arrivalDate'			=> date('d-m-Y H:i:s', $leg->arrival_time->value),
				    											  'duration'			=> $leg->duration->text,
				    											  'price'				=> NULL // Todo, add prices!
				    											);
	    				}
    				}

	    		}
    		}
    	}

    	/*
    	foreach ($agencies['agency'] as $agency) {
	    	switch ($agency['name']) {
	    		case 'DB Fernverkehr Bus':
	    			die('mofos #1');
	    			break;

	    		case 'Flixbus':
	    			die('mofos #2');
	    			break;

	    		case 'eurobahn':
	    			die('mofos #3');
	    			break;
	    		case 'NS':
	    			
	    			// $trainCrawler = $this->client->request('GET', $agency['url']);
	    			// $form = $trainCrawler->selectButton('Plannen')->form();
	    			// $trainCrawler = $this->client->submit($form, array('departure' => $data['from'], 'arrival' => $data['to']));
	    			// $trainCrawler->filter('.rp-headerPrice__amount')->each(function ($node){
	    			// 	echo $node->text();die();
	    			// });


	    			break;
	    		
	    		default:

	    			break;
	    	}
	    }*/

	    $flights = $routes['flights'];
	    $flightData = [];
	    foreach ($flights['data'] as $flight) {

	    	// Todo -> match airport name via IATA code
	    	$flightData['flights'][] = array('departure' 		=> $flight->cityFrom,
	    									 'arrival'			=> $flight->cityTo,
	    									 'departureDate'	=> date('d-m-Y H:i:s', $flight->dTime),
	    									 'arrivalDate'		=> date('d-m-Y H:i:s', $flight->aTime),
	    									 'duration'			=> $flight->fly_duration,
	    									 'price'			=> $flight->price,
	    									 'airline'			=> $flight->airlines,
	    									 'bookingLink'		=> $flight->deep_link
	    									);
	    }

	    
	    $data = [$flightData, $transitData];
	    return $data;

    }


}
