<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>Laravel</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    </head>
    <body>

        <div class="container">
            <div class="row">
                <div class=".col-md-6 .offset-md-4">
                    <form class="form-inline" id="travelForm">
                      <label class="sr-only" for="from">Vanaf</label>
                      <input type="text" class="form-control mb-2 mr-sm-2 mb-sm-0" id="from" placeholder="Vanaf">

                      <label class="sr-only" for="to">Naar</label>
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <!--<div class="input-group-addon">@</div>-->
                        <input type="text" class="form-control" id="to" placeholder="Naar" aria-describedby="passwordHelpBlock">
                      </div>

                      <label class="sr-only" for="dateFrom">Datum</label>
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <!--<div class="input-group-addon">@</div>-->
                        <input type="date" class="form-control" id="dateFrom" placeholder="Datum" aria-describedby="passwordHelpBlock">
                      </div>

                      <label class="sr-only" for="dateTo">Datum</label>
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <!--<div class="input-group-addon">@</div>-->
                        <input type="date" class="form-control" id="dateTo" placeholder="Datum" aria-describedby="passwordHelpBlock">
                      </div>

                      <label class="sr-only" for="passengers">Personen</label>
                      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <!--<div class="input-group-addon">@</div>-->
                        <input type="number" class="form-control" id="passengers" placeholder="Personen">
                      </div>


                      <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
        
    </body>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script type="text/javascript">

        $(document).ready(function() {

            $('#travelForm').on('submit', function(e) {
                e.preventDefault();

                var from        = $("#from").val();
                var to          = $("#to").val();
                var dateFrom    = $("#dateFrom").val();
                var dateTo      = $("#dateTo").val();
                var passengers  = $("#passengers").val();

                var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                       // Typical action to be performed when the document is ready:
                       console.log(xhttp.responseText);
                    }
                };

                var formData = {
                                from: from, 
                                to: to, 
                                dateFrom: dateFrom, 
                                dateTo: dateTo, 
                                passengers: passengers,
                                gMaps: xhttp.responseText
                            };

                xhttp.open("GET", "https://maps.googleapis.com/maps/api/directions/json?mode=transit&origin="+ from +"&destination="+ to +"&key=AIzaSyAoYsx2_imp0yReoUFdGkHvK2F3tZ6_EjM", true);
                xhttp.send();

                console.log(formData);

                // $.ajax({
                //     type: 'post',
                //     url: 'gmaps/directions',
                //     data: formData,
                //     // Work with the response
                //     success: function( response ) {
                //         console.log(formData);
                //     }
                // });

            });
        });

    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
</html>
